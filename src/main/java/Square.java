/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author akemi
 */
public class Square {

    private double n;

    public Square(double n) {
        this.n = n;

    }

    public double squArea() {
        return n * n;
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        if (n <= 0) {
            System.out.println("Error: Radius must more than zero!");
        }
        this.n = n;
    }

    @Override
    public String toString() {
        return "Area of square1 (n = " + this.getN() + ") is " + this.squArea();
    }
}
